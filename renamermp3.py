#!/usr/bin/env python3
# coding=utf8

""" renamermp3.py
A Simple mp3 audiobook renamer v.1.1
© 2017 Nicolas Vaughan
Runs on Python 3+
"""

import os
import sys
import shutil


try:
    from tqdm import tqdm
except ImportError:
    print('Warning: tqdm module not available')
    tqdm_exists = False
else:
    tqdm_exists = True

try:
    from colorama import init, Fore
except ImportError:
    print('Warning: colorama module not available')

    class AnsiCodes(object):
        def __init__(self):
            # the subclasses declare class attributes which are numbers.
            # Upon instantiation we define instance attributes, which are the same
            # as the class attributes but wrapped with the ANSI escape sequence
            for name in dir(self):
                if not name.startswith('_'):
                    value = getattr(self, name)

    class AnsiFore(AnsiCodes):
        BLACK = ''
        RED = ''
        GREEN = ''
        YELLOW = ''
        BLUE = ''
        MAGENTA = ''
        CYAN = ''
        WHITE = ''
        RESET = ''

    Fore = AnsiFore()

    colorama_exists = False
else:
    colorama_exists = True

# -----------------------------------------------------------

default_ext = '.mp3'
directory = ''                      # leave empty for CWD
fnames = os.listdir(directory+'.')
book_title = ""

# -----------------------------------------------------------


def get_files():
    """ Returns a list of the renamable files. """
    file_list = []
    for file in fnames:
        if os.path.splitext(file)[1] == default_ext:  # check whether file has required extension
            file_list += [file]
    file_list = sorted(file_list)
    if len(file_list) == 0:
        print(Fore.RED + "Error: No files to rename")
        exit(0)
    if len(file_list) > 999:
        print(Fore.RED + "Error: Max number of files to rename exceeds 1000")
        exit(0)
    return file_list


def set_names(orig_flist):
    """ Determines the new files names. """
    file_list = []
    num_files = len(orig_flist)
    leadzero = 0
    for i in range(num_files):
        if num_files > 99:  # range: 100--999
            if i < 9:
                leadzero = 2
            elif i < 99:
                leadzero = 1
            elif i < 1000:
                leadzero = 0
        elif num_files > 9:  # range: 10--99
            if i < 9:
                leadzero = 1
            elif i < 99:
                leadzero = 0
        elif num_files < 10:  # range: 1--9
            leadzero = 1
        file_list.append((leadzero * '0') + str(i + 1) + ' - ' + book_title)
    return file_list


def yn_question(question):
    return clean_answer(input(Fore.BLUE + question))


def clean_answer(answer):
    """ Cleans the answer string. """
    answer = answer.strip()
    answer = answer.lower()
    return answer


def check_answer(answer, bias):
    """ Checks the answer. """
    if (answer=='y') or (answer=='' and bias=='y') or (answer != 'y' and answer != 'n' and bias=='y'):
        return 'y'
    elif (answer=='n') or (answer=='' and bias=='n') or (answer != 'y' and answer != 'n' and bias=='n'):
        return 'n'


def usage():
    """ Prints the usage. """
    print(Fore.BLUE + "Usage: renamer.py book-title")


def confirmation(orig_flist, dest_flist):
    """ Asks for confirmation."""
    for input, output in zip(orig_flist, dest_flist):
        print(Fore.RED + input, Fore.WHITE + ' --> ', Fore.GREEN + output + default_ext)
    answer = yn_question('\nRename ' + str(len(orig_flist)) + ' files? [Y/n] ')
    if check_answer(answer, 'y') == 'y':
        return True
    else:
        return False


def create_backup(orig_flist):
    """ Creates a backup of the orig files. """
    answer = yn_question('Create backup? [N/y] ')
    if check_answer(answer, 'n') == 'y':
        bak_dir = directory + book_title + '.bak/'
        if not os.path.exists(bak_dir):
            os.makedirs(bak_dir)

            message = ''
            if tqdm_exists:
                bar = tqdm(total=len(orig_flist), desc=message, mininterval=0.1, unit_scale=True)
            else:
                print(message)

            for file in orig_flist:
                input_file = directory + file
                output_file = bak_dir + file
                if tqdm_exists:
                    bar.update()
                try:
                    shutil.copyfile(input_file, output_file)
                except OSError as err:
                    print(Fore.RED + 'OS error: {0}'.format(err))
                    exit(0)
        else:
            print(Fore.RED + 'Backup directory already exists! Quitting...')
            exit(0)



def rename_files(orig_flist, dest_flist):
    """ Renames the files."""
    message = ''
    if tqdm_exists:
        bar = tqdm(total=len(orig_flist), desc=message, mininterval=0.1, unit_scale=True)
    else:
        print(message)

    for in_file, out_file in zip(orig_flist, dest_flist):
        input_file = directory + in_file
        output_file = directory + out_file + default_ext
        if tqdm_exists:
            bar.update()
        try:
            os.rename(input_file, output_file)
        except OSError as err:
            print(Fore.RED + 'OS error: {0}'.format(err))
            exit(0)
    return True


def main():
    """ Main function. """

if colorama_exists:
    init()


if len(sys.argv) < 2:
    usage()
    sys.exit(-1)
elif len(sys.argv) > 2:
  for i in range(1, len(sys.argv)):
    book_title += sys.argv[i] + ' '
  book_title = book_title[0:(len(book_title)-1)]  # strip last space
else:
  book_title = sys.argv[1]

orig_flist = get_files()
dest_flist = set_names(get_files())
if confirmation(orig_flist, dest_flist):
    create_backup(orig_flist)
    if rename_files(orig_flist, dest_flist):
        print(Fore.GREEN + str(len(orig_flist)) + ' file(s) successfully renamed.')
else:
    print(Fore.RED + 'Cancelled')


if __name__ == "__main__":
    main()

